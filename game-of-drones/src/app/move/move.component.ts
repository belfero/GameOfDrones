import { Component, OnInit, Input } from '@angular/core';
import { GameService } from '../services/game.service';

@Component({
  selector: 'app-move',
  templateUrl: './move.component.html',
  styleUrls: ['./move.component.css']
})
export class MoveComponent implements OnInit {
  private availableMoves = [];
  private roundNumber;

  private currentPlayer;
  private isPlayer1Turn = true;
  private roundWinners = [];

  @Input() selectedMove;
  constructor(
    private gameService: GameService
  ) { }

  ngOnInit() {
    this.getRoundNumber();
    this.getAvailableMoves();
    this.setCurrentPlayer();
  }

  getRoundNumber(): number {
    return this.roundNumber = this.gameService.getRoundNumber();
  }

  setCurrentPlayer(): void {
    this.currentPlayer = this.gameService.getCurrentPlayer();
  }

  switchTurns(): void {
    this.isPlayer1Turn = this.gameService.switchTurns();
    this.setCurrentPlayer();
  }

  getAvailableMoves(): void {
    this.gameService.getAvailableMoves().subscribe(
      res => {
        this.availableMoves = res;
      });
  }

  updateRoundNumber(): void {
    this.roundNumber = this.gameService.incrementRoundNumber();
  }

}
