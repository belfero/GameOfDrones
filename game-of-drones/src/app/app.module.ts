import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { PlayersComponent } from './players/players.component';
import { MoveComponent } from './move/move.component';
import { WinnerComponent } from './winner/winner.component';
import { AppRoutingModule } from './/app-routing.module';
import { ScoreComponent } from './score/score.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { MoveChooserComponent } from './move-chooser/move-chooser.component';
import { RoundComponent } from './round/round.component';
import { TurnComponent } from './turn/turn.component';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MatInputModule, MatFormFieldModule, MatButtonModule, MatSelectModule } from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    PlayersComponent,
    MoveComponent,
    WinnerComponent,
    ScoreComponent,
    StatisticsComponent,
    MoveChooserComponent,
    RoundComponent,
    TurnComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatSelectModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
