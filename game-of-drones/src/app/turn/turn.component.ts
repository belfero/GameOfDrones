import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-turn',
  templateUrl: './turn.component.html',
  styleUrls: ['./turn.component.css']
})
export class TurnComponent implements OnInit {
  private playerName = '';
  constructor() { }

  @Input() data;
  ngOnInit() {
  }

}
