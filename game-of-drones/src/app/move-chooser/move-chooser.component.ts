import { Component, OnInit, Input } from '@angular/core';
import { GameService } from '../services/game.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-move-chooser',
  templateUrl: './move-chooser.component.html',
  styleUrls: ['./move-chooser.component.css']
})
export class MoveChooserComponent implements OnInit {
  private selectedMove;
  private roundNumber = 1;
  private gameData;
  private isPlayer1Turn = true;
  private playerName = localStorage.getItem('player1');

  @Input() moves;
  constructor(
    private gameService: GameService,
    private router: Router
  ) { }

  ngOnInit(
  ) {
    this.getRoundNumber();
    this.setPlayerName();
  }

  moveIsNotEmpty() {
    return this.selectedMove.trim() != '';
  }

  play(move: string): void {

    if (this.moveIsNotEmpty()) {
      this.gameService.postMove(move).subscribe(
        (res: object) => {
          this.gameData = res;
          if (!this.isPlayer1Turn) {
            this.roundNumber++;
            this.isPlayer1Turn = true;
            this.gameService.addRoundWinner(this.gameData.roundWinners.pop());
          } else {
            this.isPlayer1Turn = false;
          }
          this.setPlayerName();

          if (this.gameIsOver()) {
            this.setGameWinner(this.gameData.winner);
            console.log("the winner is " + this.gameData.winner);
            this.gameData.gameOver = false;
            this.gameData.winner = '';
            this.router.navigate(['gameOver']);
          }
        }
      );
    }
  }

  setPlayerName() {
    if (this.isPlayer1Turn) {
      this.playerName = localStorage.getItem('player1');
    } else {
      this.playerName = localStorage.getItem('player2');
    }
  }

  getRoundNumber() {
    this.roundNumber = this.gameService.getRoundNumber();
  }

  setGameWinner(winner) {
    this.gameService.setGameWinner(winner);
  }

  gameIsOver() {
    return this.gameData.gameOver && this.gameData.roundNumber == 1;
  }

}
