import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoveChooserComponent } from './move-chooser.component';

describe('MoveChooserComponent', () => {
  let component: MoveChooserComponent;
  let fixture: ComponentFixture<MoveChooserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoveChooserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoveChooserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
