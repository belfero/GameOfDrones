import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GameService } from '../services/game.service';

@Component({
  selector: 'app-winner',
  templateUrl: './winner.component.html',
  styleUrls: ['./winner.component.css']
})
export class WinnerComponent implements OnInit {
  winner: string;

  constructor(
    private router: Router,
    private gameService: GameService
  ) { }

  ngOnInit() {
    this.getGameWinner();
  }

  getGameWinner() {
    this.winner = this.gameService.getGameWinner();
  }

  playAgain(): void {
    this.gameService.restartGame().subscribe(
      (res) => {
        this.winner = '';
        localStorage.setItem('player1', '');
        localStorage.setItem('player2', '');
        this.gameService.eraseRoundWinners();
        this.router.navigate(['home']);
      }
    );

  }


}
