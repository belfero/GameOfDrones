import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlayersComponent } from './players/players.component';
import { MoveComponent } from './move/move.component';
import { WinnerComponent } from './winner/winner.component';

const routes: Routes = [
  { path: '', component: PlayersComponent, pathMatch: 'full' },
  { path: 'home', component: PlayersComponent },
  { path: 'selectMove', component: MoveComponent },
  { path: 'gameOver', component: WinnerComponent }
]

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)]
})

export class AppRoutingModule { }
