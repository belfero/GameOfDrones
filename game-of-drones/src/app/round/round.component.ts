import { Component, OnInit, Input } from '@angular/core';
import { GameService } from '../services/game.service';

@Component({
  selector: 'app-round',
  templateUrl: './round.component.html',
  styleUrls: ['./round.component.css']
})
export class RoundComponent implements OnInit {

  @Input() round;
  constructor(
    private gameService: GameService
  ) { }

  ngOnInit(
  ) {
  }

  getRoundNumber (){
    this.round = this.gameService.getRoundNumber();
  }

}
