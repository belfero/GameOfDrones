import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GameService } from '../services/game.service';

@Component({
  selector: 'app-players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.css']
})
export class PlayersComponent implements OnInit {
  private players = {
    player1: '',
    player2: ''
  };

  constructor(
    private router: Router,
    private gameService: GameService
  ) { }

  ngOnInit() {
  }

  startGame(): void {
    if (this.namesAreNotEmpty()) {
      localStorage.setItem('player1', this.players.player1);
      localStorage.setItem('player2', this.players.player2);

      this.router.navigate(['selectMove']);
    }
  }

  showStatistics(): void {
    this.router.navigate(['statistics']);
  }

  namesAreNotEmpty() {
    return this.players.player1.trim() != '' && this.players.player2.trim() != '';
  }

}
