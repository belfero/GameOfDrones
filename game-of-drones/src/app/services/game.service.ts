import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  private rulesUrl = environment.webApiUrl.concat('/rules');
  private movesUrl = environment.webApiUrl.concat('/moves');
  private gameInfoUrl = environment.webApiUrl.concat('/gameInfo');
  private playersUrl = environment.webApiUrl.concat('/players');

  private roundNumber = 1;
  private isPlayer1Turn = true;
  private moves = [];
  private roundWinners = [];
  private gameWinner = '';
  private players = [
    {
      name: localStorage.getItem('player1'),
      wins: 0
    },
    {
      name: localStorage.getItem('player2'),
      wins: 0
    }
  ];

  constructor(
    private http: HttpClient
  ) { }

  getAvailableMoves() {
    return this.http.get<Array<string>>(this.rulesUrl);
  }

  getRoundNumber(): number {
    return this.roundNumber;
  }

  incrementRoundNumber(): number {
    return ++this.roundNumber;
  }

  getCurrentPlayer(): string {
    if (this.isPlayer1Turn) {
      return this.players[0].name;
    } else {
      return this.players[1].name;
    }
  }

  switchTurns(): boolean {
    this.isPlayer1Turn ? this.isPlayer1Turn = false : this.isPlayer1Turn = true;
    return this.isPlayer1Turn;
  }

  play(move: string): void {
    this.moves.push(move);
  }

  postMove(move): Observable<object> {
    const body = {
      'move': move,
      'player1': localStorage.getItem('player1'),
      'player2': localStorage.getItem('player2'),
    };
    return this.http.post(this.movesUrl, body);
  }

  setGameWinner(winner) {
    this.gameWinner = winner;
  }

  getGameWinner() {
    return this.gameWinner;
  }

  restartGame(): Observable<any> {
    const cleanData = [];
    return this.http.put(this.gameInfoUrl, cleanData);
  }

  getRoundWinners() {
    return this.roundWinners;
  }

  addRoundWinner(winner) {
    this.roundWinners.push(winner);
  }

  eraseRoundWinners() {
    this.roundWinners = [];
  }

}
