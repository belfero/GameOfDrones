import { Component, OnInit } from '@angular/core';
import { GameService } from '../services/game.service';

@Component({
  selector: 'app-score',
  templateUrl: './score.component.html',
  styleUrls: ['./score.component.css']
})
export class ScoreComponent implements OnInit {

  private scores = [];

  constructor(
    private gameService: GameService
  ) { }

  ngOnInit() {
    this.getRoundWinners();
  }

  getRoundWinners() {
    this.scores = this.gameService.getRoundWinners();
  }


}
