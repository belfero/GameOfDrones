# Game Of Drones

Instrucciones para ejecutar servidor node:
1. cambiar al directorio '....\GOD\node-api'
2. ejecutar 'node server.js'. El servidor se ejecutará en el puerto 8000.

Instrucciones para ejecutar la aplicación web Angular:
1. En el caso de no tener instalado Angular CLI, ejecutar 'npm install -g @angular/cli'.
2. Cambiar al directorio '...\GOD\game-of-drones'
3. Ejecutar 'npm install' para instalar las dependencias
3. Ejecutar 'ng serve -o'
4. La aplicacion web se ejecutará por defecto en el puerto 4200. 
