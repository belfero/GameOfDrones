const gameRules = require('./moves');
const gameData = require('./gamedata');

let isPlayer1Turn = true;
let rules;
let p1Move;
let p2Move;

exports.game = function (data) {

    gameData.moves.push(data.move);

    if (!isPlayer1Turn) {
        p1Move = gameData.moves[0];
        p2Move = gameData.moves[1];
        rules = gameRules.moves;

        if (p1Move == p2Move) {
            itIsATie();
        }
        else {
            someoneWins(data);
        }
        console.log(gameData);
        gameData.roundNumber++;
        gameData.moves = [];
    }
    console.log(gameData);
    switchTurns();
    if (gameIsOver()) {
        console.log('game over');
        finishGame();
        setGameWinner();
        console.log(gameData);
        clearData();
    }
    return gameData;
}

exports.restartGame = function () {
    gameData.gameOver = false;
    gameData.winner = "";
}

function switchTurns() {
    isPlayer1Turn ? isPlayer1Turn = false : isPlayer1Turn = true;
}

function clearData() {
    gameData.roundNumber = 1;
    gameData.player1.victories = 0;
    gameData.player2.victories = 0;
    gameData.roundWinners = [];
}

function itIsATie() {
    console.log("It's a tie!");
    gameData.roundWinners.push("Tie");
}

function someoneWins(data) {
    if (p1BeatsP2()) {
        player1Wins(data);
    } else if (p2BeatsP1()) {
        player2Wins(data);
    } else {
        itIsATie();
    }

    console.log(`The round winner is ${gameData.roundWinners[gameData.roundNumber - 1]}`);
}

function gameIsOver() {
    return gameData.player1.victories == 3 || gameData.player2.victories == 3;
}

function setGameWinner() {
    gameData.player1.victories == 3 ? gameData.winner = gameData.player1.name : gameData.winner = gameData.player2.name;
}

function finishGame() {
    gameData.gameOver = true;
}

function p1BeatsP2() {
    let result = false;
    rules.forEach(rule => {
        if (rule.move == p1Move && rule.kills == p2Move)
            result = true
    });
    return result;
}

function p2BeatsP1() {
    let result = false;
    rules.forEach(rule => {
        if (rule.move == p2Move && rule.kills == p1Move)
            result = true
    });
    return result;
}

function player1Wins(data) {
    console.log('Player 1 wins');
    gameData.player1.victories++;
    gameData.player1.name = data.player1;
    gameData.roundWinners.push(gameData.player1.name);
}

function player2Wins(data) {
    console.log('Player 2 wins');
    gameData.player2.victories++;
    gameData.player2.name = data.player2;
    gameData.roundWinners.push(gameData.player2.name);
}