let gameRules = require('../../moves');
var gameModule = require('../../game');
var game = gameModule.game;
var restart = gameModule.restartGame;

module.exports = function (app, db) {

    app.post('/moves', (req, res) => {
        console.log('The move was ' + req.body.move);
        let gameData = game(req.body);
        res.send(gameData);
    });

    app.put('/gameInfo', (req, res) => {
        restart();
        res.send([]);
    });

    app.get('/rules', (req, res) => {
        let moveRules = gameRules.moves;
        let moves = [];

        moveRules.forEach(rule => {
            moves.push(rule.move);
        });
        res.send(moves);
    });

};