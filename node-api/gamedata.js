module.exports = {
    player1: {
        name: "",
        victories: 0
    },
    player2: {
        name: "",
        victories: 0
    },
    moves: [],
    roundWinners: [],
    roundNumber: 1,
    gameOver: false,
    winner: ''
};